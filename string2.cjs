function string2(teststring2) {

    var expression = /^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/gm;

    if (expression.test(teststring2)) {

        var object1 = [];
        var ipaddress = teststring2.split('.');

        for (let i in ipaddress) {

            ipaddress[i] = parseInt(ipaddress[i]);
            object1.push(ipaddress[i]);

        }
        return object1;
    }
    else {
        return [];
    }
}
module.exports = string2;